﻿using System.Web.Optimization;

namespace JuiceMachine
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/js").Include(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/bootstrap.js",
                "~/Scripts/bootbox.all.js",
                "~/Scripts/Site.js"));

            bundles.Add(new StyleBundle("~/Admin/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/fontawesome-all.css",
                "~/Content/brands.css",
                "~/Content/Admin.css"));

            bundles.Add(new StyleBundle("~/Admin/js").Include(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/jquery.validate*",
                "~/Scripts/bootstrap.js",
                "~/Scripts/bootbox.all.js",
                "~/Scripts/Admin.js"));
        }
    }
}