﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using JuiceMachine.Areas.Admin.Models;
using JuiceMachine.Code.Filters;
using JuiceMachine.Models;
using JuiceMachine.Models.DbModels;
using Microsoft.Ajax.Utilities;

namespace JuiceMachine.Areas.Admin.Controllers
{
    [AjaxOnly]
    [AdminAuth]
    public class ApiController : Controller
    {
        private readonly JuiceMachineContext _dbContext;

        public ApiController()
        {
            _dbContext = new JuiceMachineContext();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _dbContext.Dispose();
            }

            base.Dispose(disposing);
        }


        [HttpGet]
        public ActionResult GetJuiceList()
        {
            var juiceList = _dbContext.JuiceItems.ToList();
            return PartialView(juiceList);
        }

        [HttpGet]
        public ActionResult GetCoinList()
        {
            var coinList = _dbContext.CoinItems.ToList();
            return PartialView(coinList);
        }

        [HttpGet]
        public ActionResult EditCoin(int id)
        {
            var coin = _dbContext.CoinItems.FirstOrDefault(c => c.Id == id);
            if (coin == null)
                return HttpNotFound();
            ViewBag.Par = coin.Par;
            return PartialView(new EditCoinModel() {Id = coin.Id, Balance = coin.Balance, IsBlocked = coin.IsBlocked});
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditCoin(EditCoinModel model)
        {
            if (!ModelState.IsValid)
                return Json(new {@success = false});
            var coin = _dbContext.CoinItems.FirstOrDefault(c => c.Id == model.Id);
            if (coin == null)
                return Json(new {@success = false});
            coin.Balance = model.Balance;
            coin.IsBlocked = model.IsBlocked;
            _dbContext.SaveChanges();
            return Json(new {@success = true});
        }

        [HttpGet]
        public ActionResult CreateJuice()
        {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateJuice(CreateJuiceModel model)
        {
            if (!ModelState.IsValid)
                return Json(new {@success = false});
            var juice = new JuiceItem()
            {
                PriceValue = model.PriceValue, StockBalance = model.StockBalance, Title = model.Title,
                ImageSrc = model.Image
            };
            _dbContext.JuiceItems.Add(juice);
            _dbContext.SaveChanges();
            return Json(new {@success = true});
        }

        [HttpPost]
        public JsonResult UploadImage(object image)
        {
            #region удаление файлов, которые не привязаны к напиткам

            var uploadPath = Server.MapPath("~/Upload");
            var allUploadedNames = new List<string>(Directory.GetFiles(uploadPath))
                .Select(Path.GetFileName).ToList();
            var allNamesInDb =
                _dbContext.JuiceItems.Select(i => i.ImageSrc).ToList().Select(i =>
                    i.Substring(i.LastIndexOf("/", StringComparison.Ordinal) + 1)).ToList();
            allUploadedNames.Except(allNamesInDb).ForEach(i => System.IO.File.Delete(Path.Combine(uploadPath, i)));

            #endregion

            #region загрузка файла

            var length = Request.ContentLength;
            var bytes = new byte[length];
            Request.InputStream.Read(bytes, 0, length);
            var fileName = Request.Headers["X-File-Name"];
            var saveToFileLoc = Server.MapPath("~/Upload/" + fileName);
            using (var fileStream = new FileStream(saveToFileLoc, FileMode.Create, FileAccess.ReadWrite))
            {
                fileStream.Write(bytes, 0, length);
            }

            #endregion

            return Json(new {success = true, resultPath = "/Upload/" + fileName});
        }

        [HttpGet]
        public ActionResult EditJuice(int id)
        {
            var juice = _dbContext.JuiceItems.FirstOrDefault(c => c.Id == id);
            if (juice == null)
                return HttpNotFound();
            return PartialView(new EditJuiceModel()
            {
                Id = juice.Id, Image = juice.ImageSrc, Title = juice.Title, PriceValue = juice.PriceValue,
                StockBalance = juice.StockBalance
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EditJuice(EditJuiceModel model)
        {
            if (!ModelState.IsValid)
                return Json(new {success = false});
            var juice = _dbContext.JuiceItems.FirstOrDefault(c => c.Id == model.Id);
            if (juice == null)
                return Json(new {success = false});
            juice.Title = model.Title;
            juice.ImageSrc = model.Image;
            juice.PriceValue = model.PriceValue;
            juice.StockBalance = model.StockBalance;
            _dbContext.SaveChanges();
            return Json(new {success = true});
        }

        [HttpPost]
        public JsonResult DeleteJuice(int id)
        {
            var juice = _dbContext.JuiceItems.FirstOrDefault(c => c.Id == id);
            if (juice == null)
                return Json(new {@success = false});
            _dbContext.JuiceItems.Remove(juice);
            _dbContext.SaveChanges();
            return Json(new {success = true});
        }
    }
}