﻿using System.Web.Mvc;
using JuiceMachine.Code.Filters;

namespace JuiceMachine.Areas.Admin.Controllers
{
    [AdminAuth]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}