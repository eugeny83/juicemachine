﻿using System.ComponentModel.DataAnnotations;

namespace JuiceMachine.Areas.Admin.Models
{
    public class CreateJuiceModel
    {
        [Display(Name = "Заголовок")]
        [Required(ErrorMessage = "Обязательно для заполнения")]
        [MaxLength(255, ErrorMessage = "Максимальная длина 255 символов")]
        public string Title { get; set; }

        [Display(Name = "Картинка")]
        [Required(ErrorMessage = "Обязательно для заполнения")]
        public string Image { get; set; }

        [Display(Name = "Цена")]
        [Required(ErrorMessage = "Обязательно для заполнения")]
        [Range(1, 100, ErrorMessage = "Допустимое значение от 1 до 100")]
        [RegularExpression("^[\\d]+$", ErrorMessage = "Допустимы только цифры")]
        public int PriceValue { get; set; }

        [Display(Name = "Количество в автомате")]
        [Required(ErrorMessage = "Обязательно для заполнения")]
        [Range(0, 1000, ErrorMessage = "Допустимое значение от 0 до 1000")]
        [RegularExpression("^[\\d]+$", ErrorMessage = "Допустимы только цифры")]
        public int StockBalance { get; set; }
    }
}