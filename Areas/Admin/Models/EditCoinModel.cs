﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace JuiceMachine.Areas.Admin.Models
{
    public class EditCoinModel
    {
        [HiddenInput(DisplayValue = false)]
        [Required]
        public int Id { get; set; }

        [Display(Name = "Статус блокировки")]
        [Required]
        public bool IsBlocked { get; set; }

        [Display(Name = "Количество в автомате")]
        [Required(ErrorMessage = "Обязательно для заполнения")]
        [Range(0, 1000, ErrorMessage = "Допустимое значение от 0 до 1000")]
        [RegularExpression("^[\\d]+$", ErrorMessage = "Допустимы только цифры")]
        public int Balance { get; set; }
    }
}