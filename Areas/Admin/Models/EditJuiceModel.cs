﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace JuiceMachine.Areas.Admin.Models
{
    public class EditJuiceModel : CreateJuiceModel
    {
        [HiddenInput(DisplayValue = false)]
        [Required]
        public int Id { get; set; }
    }
}