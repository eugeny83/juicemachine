﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JuiceMachine.Models;
using JuiceMachine.Models.DbModels;

namespace JuiceMachine.Code
{
    public class CoinBuffer
    {
        private readonly List<CoinPar> _sessionCoins = new List<CoinPar>();
        private int _userDebt = 0;

        private CoinBuffer()
        {
        }


        public void AddCoin(CoinPar coin)
        {
            _sessionCoins.Add(coin);
            _sessionCoins.Sort((c1, c2) => -1 * c1.CompareTo(c2));
        }

        public void BuyJuice(JuiceMachineContext dbContext, int juicePrice)
        {
            _userDebt += juicePrice;
            var coins = _sessionCoins.Where(c => c.Price() <= _userDebt).ToList(); //взять все монеты ниже задолженности
            while (coins.Any())
            {
                var firstMaxCoin = coins.First();
                _userDebt -= firstMaxCoin.Price(); //вычесть из баланса номинал монеты
                _sessionCoins.Remove(firstMaxCoin);
                coins = coins.Skip(1).ToList();
                coins = coins.Where(c => c.Price() <= _userDebt).ToList();
                dbContext.CoinItems.Single(c => c.Par == firstMaxCoin)
                    .Balance++; //и добавить эту монету на баланс автомата
                dbContext.SaveChanges();
            }
        }

        public Tuple<List<CoinPar>, int> DoMoneyback(JuiceMachineContext dbContext)
        {
            List<CoinPar> resultCoins;
            if (_userDebt == 0) //долг нулевой, возвращаем весь остаток монет
            {
                resultCoins = _sessionCoins.ToList();
                _sessionCoins.Clear();
                return new Tuple<List<CoinPar>, int>(resultCoins, 0);
            }

            //есть долг, возвращаем все кроме последней монеты, оставшуюся монету нужно разменять
            resultCoins = _sessionCoins.Take(_sessionCoins.Count - 1).ToList();
            var lastCoin = _sessionCoins.Last();
            dbContext.CoinItems.Single(c => c.Par == lastCoin).Balance++;
            dbContext.SaveChanges();
            var weDebt = lastCoin.Price() - _userDebt; //с учетом оставшейся монеты мы должны
            _sessionCoins.Clear();
            _userDebt = 0;
            var suitableCoins = dbContext.CoinItems.ToList().Where(c => c.Par.Price() <= weDebt)
                .Select(c => new Tuple<CoinPar, int>(c.Par, c.Balance))
                .ToList(); //взять из базы все подходящие для сдачи монеты
            var coinsAsItems = new List<CoinPar>();
            foreach (var coinTuple in suitableCoins)
                coinsAsItems.AddRange(Enumerable.Repeat(coinTuple.Item1, coinTuple.Item2));
            coinsAsItems =
                coinsAsItems.OrderByDescending(c => c.Price())
                    .ToList(); //преобразовать в плоский отсортированный список
            while (coinsAsItems.Any() && weDebt != 0)
            {
                var firstSuitableCoin = coinsAsItems.First();
                resultCoins.Add(firstSuitableCoin);
                weDebt -= firstSuitableCoin.Price();
                coinsAsItems = coinsAsItems.Skip(1).Where(c => c.Price() <= weDebt).ToList();
                dbContext.CoinItems.Single(c => c.Par == firstSuitableCoin).Balance--;
                dbContext.SaveChanges();
            }

            if (weDebt > 0)
                _userDebt = -1 * weDebt;
            return new Tuple<List<CoinPar>, int>(resultCoins, weDebt);
        }

        public int GetBalance()
        {
            return _sessionCoins.Sum(c => c.Price()) - _userDebt;
        }

        public static CoinBuffer Get(HttpSessionStateBase session)
        {
            if (session == null)
                throw new Exception("Сессия не найдена");
            var coinBuffer = (CoinBuffer) (session["CoinBuffer"] ?? new CoinBuffer());
            session["CoinBuffer"] = coinBuffer;
            return coinBuffer;
        }
    }
}