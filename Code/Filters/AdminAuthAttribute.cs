﻿using System;
using System.Web.Mvc;

namespace JuiceMachine.Code.Filters
{
    public class AdminAuthAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext == null)
                throw new ArgumentNullException();
            if (filterContext.HttpContext.Session["AuthAsAdmin"] != null)
                return;
            var authParam = filterContext.HttpContext.Request.QueryString["Auth"];
            if (authParam == null || authParam != "123456")
            {
                filterContext.Result = new HttpUnauthorizedResult();
                return;
            }

            filterContext.HttpContext.Session["AuthAsAdmin"] = 0;
            filterContext.Result = new RedirectResult("/Admin");
        }
    }
}