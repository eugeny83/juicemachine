﻿using System.Linq;
using System.Web.Mvc;
using JuiceMachine.Code;
using JuiceMachine.Models;
using JuiceMachine.Models.DbModels;

namespace JuiceMachine.Controllers
{
    public class ApiController : Controller
    {
        private readonly JuiceMachineContext _dbContext;

        public ApiController()
        {
            _dbContext = new JuiceMachineContext();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _dbContext.Dispose();
            }

            base.Dispose(disposing);
        }


        [HttpPost]
        public JsonResult SendCoin(int coinId)
        {
            var coinPar = _dbContext.CoinItems.Single(c => c.Id == coinId).Par;
            var coinBuffer = CoinBuffer.Get(Session);
            coinBuffer.AddCoin(coinPar);
            return Json(new {balance = coinBuffer.GetBalance()});
        }

        [HttpPost]
        public JsonResult Moneyback()
        {
            var coinBuffer = CoinBuffer.Get(Session);
            var moneybackResult = coinBuffer.DoMoneyback(_dbContext);
            return Json(new
            {
                balance = coinBuffer.GetBalance(), weDebt = moneybackResult.Item2,
                coins = moneybackResult.Item1.Select(c => c.Name()).ToArray()
            });
        }

        [HttpPost]
        public JsonResult BuyJuice(int juiceId)
        {
            var juice = _dbContext.JuiceItems.Single(i => i.Id == juiceId);
            juice.StockBalance--;
            _dbContext.SaveChanges();
            var coinBuffer = CoinBuffer.Get(Session);
            coinBuffer.BuyJuice(_dbContext, juice.PriceValue);
            return Json(new {balance = coinBuffer.GetBalance(), juiceInStock = juice.StockBalance > 0});
        }
    }
}