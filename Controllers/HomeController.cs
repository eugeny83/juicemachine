﻿using System.Linq;
using System.Web.Mvc;
using JuiceMachine.Code;
using JuiceMachine.Models;

namespace JuiceMachine.Controllers
{
    public class HomeController : Controller
    {
        private readonly JuiceMachineContext _dbContext;

        public HomeController()
        {
            _dbContext = new JuiceMachineContext();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _dbContext.Dispose();
            }

            base.Dispose(disposing);
        }


        public ActionResult Index()
        {
            var coinBuffer = CoinBuffer.Get(Session);
            ViewBag.Coins = _dbContext.CoinItems.ToList();
            ViewBag.Balance = coinBuffer.GetBalance();
            var juiceItems = _dbContext.JuiceItems.ToList();
            return View(juiceItems);
        }
    }
}