﻿namespace JuiceMachine.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CoinItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Par = c.Int(nullable: false),
                        IsBlocked = c.Boolean(nullable: false),
                        Balance = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.JuiceItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        ImageSrc = c.String(nullable: false),
                        PriceValue = c.Double(nullable: false),
                        StockBalance = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.JuiceItems");
            DropTable("dbo.CoinItems");
        }
    }
}
