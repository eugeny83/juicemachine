﻿namespace JuiceMachine.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Af : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.JuiceItems", "Title", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.JuiceItems", "PriceValue", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.JuiceItems", "PriceValue", c => c.Double(nullable: false));
            AlterColumn("dbo.JuiceItems", "Title", c => c.String(nullable: false));
        }
    }
}
