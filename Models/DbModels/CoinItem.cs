﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JuiceMachine.Models.DbModels
{
    public class CoinItem
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public CoinPar Par { get; set; }

        public bool IsBlocked { get; set; }

        public int Balance { get; set; }
    }
}