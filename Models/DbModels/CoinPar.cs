﻿using System;

namespace JuiceMachine.Models.DbModels
{
    public enum CoinPar
    {
        One,
        Two,
        Five,
        Ten
    }

    public static class CoinParExt
    {
        public static string Name(this CoinPar par)
        {
            switch (par)
            {
                case CoinPar.One: return "1р.";
                case CoinPar.Two: return "2р.";
                case CoinPar.Five: return "5р.";
                case CoinPar.Ten: return "10р.";
                default: throw new Exception("Ошибка получения заголовка у номинала монеты");
            }
        }

        public static int Price(this CoinPar par)
        {
            switch (par)
            {
                case CoinPar.One: return 1;
                case CoinPar.Two: return 2;
                case CoinPar.Five: return 5;
                case CoinPar.Ten: return 10;
                default: throw new Exception("Ошибка получения цены у номинала монеты");
            }
        }
    }
}