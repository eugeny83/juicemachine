﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JuiceMachine.Models.DbModels
{
    public class JuiceItem
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(255)]
        public string Title { get; set; }

        [Required]
        public string ImageSrc { get; set; }

        public int PriceValue { get; set; }

        public int StockBalance { get; set; }
    }
}