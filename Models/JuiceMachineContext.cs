﻿using System.Data.Entity;
using JuiceMachine.Models.DbModels;

namespace JuiceMachine.Models
{
    public class JuiceMachineContext : DbContext
    {
        public DbSet<JuiceItem> JuiceItems { get; set; }
        public DbSet<CoinItem> CoinItems { get; set; }
    }
}