﻿$(document).ready(function() {
    //jquery validate and bootstrap 4
    jQuery.validator.setDefaults({
        errorElement: 'span',
        errorPlacement: function(error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    //закрытие модального диалога
    $("body").on("hidden.bs.modal",
        "div.modal",
        function(e) {
            var rootEl = $(e.target);
            rootEl.modal("dispose");
            rootEl.remove();
        });

    //обновить грид напитков
    function updateJuiceGrid() {
        $.ajax({
            url: "/Admin/Api/GetJuiceList",
            type: "GET",
            success: function(result) {
                $("#juice-container").html(result);
            }
        });
    }

    //принудительное обновление при загрузке, событие не вызывается
    updateJuiceGrid();

    //обновить грид монет
    function updateCoinGrid() {
        $.ajax({
            url: "/Admin/Api/GetCoinList",
            type: "GET",
            success: function(result) {
                $("#coin-container").html(result);
            }
        });
    }

    //переключение вкладок грида
    $('a[data-toggle="tab"]').on("shown.bs.tab",
        function(e) {
            if (e.target.id === "juice-tab") {
                updateJuiceGrid();
            } else if (e.target.id === "coin-tab") {
                updateCoinGrid();
            };
        });

    //открытие диалога редактирования монеты
    $("body").on("click",
        "#coin-container i.edit-button",
        function() {
            var coinId = $(this).data("id");
            $.ajax({
                url: "/Admin/Api/EditCoin",
                type: "GET",
                data: { id: coinId },
                success: function(result) {
                    $("body").append(result);
                    $("#dialog-edit-coin").modal("show");
                    $("#dialog-edit-coin form").data("validator", null);
                    $.validator.unobtrusive.parse($("#dialog-edit-coin form"));
                }
            });
        });

    //сохранение диалога редактирования монеты
    $("body").on("click",
        "#dialog-edit-coin button.btn-primary",
        function(e) {
            e.preventDefault();
            var form = $("#dialog-edit-coin form");
            form.validate();
            if (!form.valid()) {
                return;
            }
            $.post(form.attr("action"),
                form.serialize(),
                function(result) {
                    if (result.success === true) {
                        $("#dialog-edit-coin").modal("hide");
                        updateCoinGrid();
                    } else if (result.success === false) {
                        console.log("Ошибка редактирования монеты");
                    }
                });
        });

    //открытие диалога создания напитка
    $("#juice-add").on("click",
        function() {
            $.ajax({
                url: "/Admin/Api/CreateJuice",
                type: "GET",
                success: function(result) {
                    $("body").append(result);
                    $("#dialog-create-juice").modal("show");
                    $("#dialog-create-juice form").data("validator", null);
                    $.validator.unobtrusive.parse($("#dialog-create-juice form"));
                }
            });
        });

    //сохранение диалога создания напитка
    $("body").on("click",
        "#dialog-create-juice .modal-footer button.btn-primary",
        function(e) {
            e.preventDefault();
            var form = $("#dialog-create-juice form");
            form.validate();
            if (!form.valid()) {
                return;
            }
            $.post(form.attr("action"),
                form.serialize(),
                function(result) {
                    if (result.success === true) {
                        $("#dialog-create-juice").modal("hide");
                        updateJuiceGrid();
                    } else if (result.success === false) {
                        console.log("Ошибка создания напитка");
                    }
                });
        });

    //загрузка картинки напитка
    $("body").on("click",
        "#juice-upload-image-button",
        function(e) {
            e.preventDefault();
            $("#hidden-juice-image").click();
        });
    $("body").on("change",
        "#hidden-juice-image",
        function(e) {
            var file = e.target.files[0];
            var xhr = new XMLHttpRequest();
            xhr.open("POST", "/Admin/Api/UploadImage", true);
            xhr.setRequestHeader("X-File-Name", file.name);
            xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest"); //иначе фильтр контроллера не пропустит
            xhr.send(file);
            xhr.onreadystatechange = function() {
                if (this.readyState === 4 && this.status === 200) {
                    var jsonResult = JSON.parse(this.responseText);
                    if (jsonResult.success === true) {
                        $("#Image").val(jsonResult.resultPath);
                    }
                }
            }
        });

    //открытие диалога редактирования напитка
    $("body").on("click",
        "#juice-container i.edit-button",
        function() {
            var juiceId = $(this).data("id");
            $.ajax({
                url: "/Admin/Api/EditJuice",
                type: "GET",
                data: { id: juiceId },
                success: function(result) {
                    $("body").append(result);
                    $("#dialog-edit-juice").modal("show");
                    $("#dialog-edit-juice form").data("validator", null);
                    $.validator.unobtrusive.parse($("#dialog-edit-juice form"));
                }
            });
        });

    //сохранение диалога редактирования напитка
    $("body").on("click",
        "#dialog-edit-juice .modal-footer button.btn-primary",
        function(e) {
            e.preventDefault();
            var form = $("#dialog-edit-juice form");
            form.validate();
            if (!form.valid()) {
                return;
            }
            $.post(form.attr("action"),
                form.serialize(),
                function(result) {
                    if (result.success === true) {
                        $("#dialog-edit-juice").modal("hide");
                        updateJuiceGrid();
                    } else if (result.success === false) {
                        console.log("Ошибка редактирования напитка");
                    }
                });
        });

    //удаление напитка
    $("body").on("click",
        "#juice-container i.delete-button",
        function() {
            var juiceId = $(this).data("id");
            bootbox.confirm({
                message: "Подтверждение удаления",
                locale: "ru",
                centerVertical: true,
                callback: function(result) {
                    if (result === true) {
                        $.post("/Admin/Api/DeleteJuice",
                            { id: juiceId },
                            function(result) {
                                if (result.success === true) {
                                    updateJuiceGrid();
                                } else if (result.success === false) {
                                    console.log("Ошибка удаления напитка");
                                }
                            });
                    }
                }
            });
        }
    );
});