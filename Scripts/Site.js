﻿$(document).ready(function() {
    //рамка у напитков при наведении
    $(document).on({
            mouseenter: function() {
                $(this).addClass("border-dark");
            },
            mouseleave: function() {
                $(this).removeClass("border-dark");
            }
        },
        ".card:not(.disabled)");

    //добавить монету
    $(".send-money-button").not(".disabled").on("click",
        function() {
            var coinId = $(this).attr("data-id");
            $.post("/Api/SendCoin",
                { coinId: coinId },
                function(result) {
                    $("#user-balance").text(result.balance);
                    $("#moneyback-button").removeClass("disabled");
                });
        });

    //сделать манибек
    $(document).on("click",
        "#moneyback-button:not(.disabled)",
        function() {
            $.post("/Api/Moneyback",
                {},
                function(result) {
                    $("#user-balance").text(result.balance);
                    $("#moneyback-button").addClass("disabled");
                    var coinsStr = "";
                    for (var i = 0; i < result.coins.length; i++) {
                        coinsStr = coinsStr + result.coins[i];
                        if (i < result.coins.length - 1) {
                            coinsStr = coinsStr + ", ";
                        }
                    }
                    var weDebtMessage = "";
                    if (result.weDebt > 0) {
                        weDebtMessage =
                            "\nК сожалению мы не можем выдать сдачу полностью в связи с отсутствием монет";
                    }
                    bootbox.alert({
                        message: "Возвращены монеты: " + coinsStr + weDebtMessage,
                        locale: "ru",
                        centerVertical: true
                    });
                });
        });

    //купить напиток
    $(document).on("click",
        ".card:not(.disabled)",
        function() {
            var juiceId = $(this).attr("data-id");
            if (parseInt($("#user-balance").text()) < parseInt($(this).find(".card-price").text())) {
                bootbox.alert({
                    message: "У вас недостаточно средств для покупки данного напитка",
                    locale: "ru",
                    centerVertical: true
                });
                return;
            }
            var cardEl = $(this);
            $.post("/Api/BuyJuice",
                { juiceId: juiceId },
                function(result) {
                    $("#user-balance").text(result.balance);
                    if (result.balance === 0) {
                        $("#moneyback-button").addClass("disabled");
                    }
                    if (!result.juiceInStock) {
                        cardEl.addClass("disabled");
                    }
                    bootbox.alert({
                        message: "Вы приобрели напиток",
                        locale: "ru",
                        centerVertical: true
                    });
                });
        });
});